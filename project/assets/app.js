/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
// require jQuery normally
/*var $ = require('jquery');
const jQuery = require('jquery');
global.$ = $;
global.jQuery = $;
*/

//$('#datetimepicker1').datetimepicker();
const $ = require('jquery');

global.$ = global.jQuery = $;

import './styles/app.scss';
require('@fortawesome/fontawesome-free/js/all.js');
import { Tooltip, Toast, Popover } from 'bootstrap';
// start the Stimulus application

import './bootstrap';

   $(function () {
             $('#datetimepicker1').datetimepicker();
         });
